//
//  Constants.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/15/22.
//

import Foundation
import Firebase

let COLLECTION_USERS = Firestore.firestore().collection("users")
