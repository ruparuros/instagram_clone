//
//  User.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/15/22.
//

import Foundation
import UIKit

struct User{
    let email:String
    let fullname:String
    let username:String
    let profileImageUrl: String
    let uid: String
    
    init(dictionary: [String: Any]){
        self.email = dictionary["email"] as? String ?? ""
        self.fullname = dictionary["password"] as? String ?? ""
        self.username = dictionary["username"] as? String ?? ""
        self.uid = dictionary["uid"] as? String ?? ""
        self.profileImageUrl = dictionary["profileImageUrl"] as? String ?? ""
    }
}
