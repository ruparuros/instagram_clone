//
//  AuthentacionViewModel.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/11/22.
//

import Foundation
import UIKit

protocol FormViewModel {
    func updateForm()
}

protocol AuthentacionViewModel{
    var formIsValid: Bool { get }
    var backgroundColor: UIColor { get }
    var buttonTitleColor: UIColor { get }
}
struct LoginViewModel: AuthentacionViewModel{
    var email: String?
    var password: String?
    
    var formIsValid: Bool {
        return email?.isEmpty == false && password?.isEmpty == false
    }
    
    var backgroundColor:UIColor{
        return formIsValid ? #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)  : #colorLiteral(red: 0.8446559906, green: 0.5145556927, blue: 1, alpha: 1).withAlphaComponent(0.5)
    }
    
    var buttonTitleColor:UIColor{
        return formIsValid ? .white : UIColor(white: 1, alpha: 0.68)
    }
}

struct RegistrationVewModel: AuthentacionViewModel{
    var email: String?
    var password: String?
    var fullName: String?
    var username: String?
    
    var formIsValid: Bool {
        return email?.isEmpty == false && password?.isEmpty == false &&
        fullName?.isEmpty == false && username?.isEmpty == false
    }
    
    var backgroundColor:UIColor{
        return formIsValid ? #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1) : #colorLiteral(red: 0.8446559906, green: 0.5145556927, blue: 1, alpha: 1).withAlphaComponent(0.5)
    }
    
    var buttonTitleColor:UIColor{
        return formIsValid ? .white : UIColor(white: 1, alpha: 0.68)
    }
}
