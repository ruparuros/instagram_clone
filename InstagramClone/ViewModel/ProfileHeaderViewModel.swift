//
//  ProfileHeaderViewModel.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/15/22.
//

import Foundation

struct ProfileHeaderViewModel{
    
    let user:User
    
    var fullname:String{
        return user.fullname
    }
    
    var profileImageUrl:URL?{
        return URL(string: user.profileImageUrl)
    }
    init(user: User){
        self.user = user
    }
}
