//
//  FeedCell.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/6/22.
//

import UIKit

class FeedCell: UICollectionViewCell {
   
    @IBOutlet weak var profilImage: UIImageView!
    @IBOutlet weak var usernameTitleButton: UIButton!
    @IBOutlet weak var postImage: UIImageView!
    

    override func layoutSubviews() {
        profilImage.clipsToBounds = true
        profilImage.layer.cornerRadius = profilImage.frame.width / 2
        usernameTitleButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
    }
}
