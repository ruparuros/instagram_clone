//
//  ProfileCell.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/14/22.
//

import UIKit

class ProfileCell: UICollectionViewCell {
    
    private let postImageView: UIImageView = {
        let iv  = UIImageView()
        iv.image = #imageLiteral(resourceName: "1646220094097")
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .lightGray
        
        
        addSubview(postImageView)
        postImageView.fillSuperview()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
  
}
