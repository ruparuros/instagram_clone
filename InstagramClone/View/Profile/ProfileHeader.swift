//
//  ProfileHeader.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/14/22.
//

import UIKit
import SDWebImage
class ProfileHeader: UICollectionReusableView {
        
    
    //MARK: - PROPERTIES
    
    var viewModel: ProfileHeaderViewModel? {
        didSet{
            configure()
        }
    }
    
    
    private let profileImage: UIImageView = {
        let iv  = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = .lightGray
        return iv
    }()
    
    private let nameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    private lazy var editProfileFollowButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Edit Profile", for: .normal)
        button.layer.cornerRadius = 3
        button.layer.borderWidth = 0.5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(handleEditFollowProfileTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var postLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.attributedText = attributedStateText(value: 187, label: "post")
        label.textAlignment = .center
        return label
    }()
    
    private lazy var followersLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.attributedText = attributedStateText(value: 800, label: "followers")
        label.textAlignment = .center
        return label
    }()
    
    private lazy var followingLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.attributedText = attributedStateText(value: 756, label: "following")
        label.textAlignment = .center
        
        return label
    }()
    
    let gridButton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "square.grid.3x3"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        return button
    }()
    
    let listBUtton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "list.bullet"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        return button
    }()
    
    let bookmarkButton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "ribbon"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        return button
    }()
  
    //MARK: - LIFECYCLE
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white

        addSubview(profileImage)
        profileImage.anchor(top: topAnchor,left: leftAnchor,paddingTop: 16,paddingLeft: 12)
        profileImage.setDimensions(height: 80, width: 80)
        profileImage.layer.cornerRadius = 80 / 2
        
        
        addSubview(nameLabel)
        nameLabel.anchor(top:profileImage.bottomAnchor, left: leftAnchor,paddingTop: 12,paddingLeft: 12)
        
        addSubview(editProfileFollowButton)
        editProfileFollowButton.anchor(top:nameLabel.bottomAnchor,left: leftAnchor,right: rightAnchor,paddingTop: 16,paddingLeft: 24,paddingRight: 24)
        
        let stack = UIStackView(arrangedSubviews: [postLabel,followersLabel,followingLabel])
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.anchor(left:profileImage.rightAnchor,right: rightAnchor,paddingLeft: 12,paddingRight: 12,height: 100)
        
        let topDivider = UIView()
        topDivider.backgroundColor = .systemGray2
        let bottomDivider = UIView()
        bottomDivider.backgroundColor = .systemGray2
        
        let buttonStack = UIStackView(arrangedSubviews: [gridButton,listBUtton,bookmarkButton])
        buttonStack.distribution = .fillEqually
        addSubview(buttonStack)
        addSubview(topDivider)
        addSubview(bottomDivider)
        buttonStack.anchor(left:leftAnchor,bottom: bottomAnchor,right: rightAnchor,height: 50)
        topDivider.anchor(top:buttonStack.topAnchor,left: leftAnchor,right: rightAnchor,height: 0.5)
        bottomDivider.anchor(top:buttonStack.bottomAnchor,left: leftAnchor,right: rightAnchor,height: 0.5)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc func handleEditFollowProfileTapped(){
        
    }
    
    
    func configure(){
        guard let viewModel = viewModel else {
            return
        }
        
        nameLabel.text = viewModel.fullname
        profileImage.sd_setImage(with: viewModel.profileImageUrl)

    }
    
    func attributedStateText(value: Int,label: String) -> NSAttributedString {
        let attributedText = NSMutableAttributedString(string: "\(value)\n", attributes: [.font:UIFont.boldSystemFont(ofSize: 14)])
        attributedText.append(NSAttributedString(string: label, attributes: [.font: UIFont.systemFont(ofSize: 14), .foregroundColor: UIColor.lightGray]))
        
        return attributedText
    }
    
 
   
}
