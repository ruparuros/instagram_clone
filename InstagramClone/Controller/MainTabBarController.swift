//
//  MainTabBarController.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/6/22.
//

import UIKit
import Firebase


class MainTabBarController: UITabBarController {

// MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViewControllers()
        checkIfUserIsLogedIn()
        //logout()
        //self.navigationController?.navigationBar.backgroundColor  = .red
    }
    
    // MARK: - Api
    
    func checkIfUserIsLogedIn(){
        
        if Auth.auth().currentUser == nil {
           
            DispatchQueue.main.async {
                let controller = LoginVC()
                let nav = UINavigationController(rootViewController: controller)
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
   
    // MARK: - Helpers
    func configureViewControllers(){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        
        let feed = templateNavigationController(unselectedImage: #imageLiteral(resourceName: "home_unselected"),selectedImage: #imageLiteral(resourceName: "home_selected"),rootViewController: storyBoard.instantiateViewController(withIdentifier: "FeedVc") as! FeedVc)
        
        let search = templateNavigationController(unselectedImage: #imageLiteral(resourceName: "search_unselected"),selectedImage: #imageLiteral(resourceName: "search_selected"),rootViewController: SearchVC())
        
        let imagesSelector = templateNavigationController(unselectedImage: #imageLiteral(resourceName: "plus_unselected"),selectedImage: #imageLiteral(resourceName: "plus_unselected"),rootViewController: ImageSelectorVC())
        
        let notification = templateNavigationController(unselectedImage: #imageLiteral(resourceName: "like_unselected"),selectedImage: #imageLiteral(resourceName: "like_selected"),rootViewController: NotificationVC())
        
        let profileLayout = UICollectionViewFlowLayout()
        let profile = templateNavigationController(unselectedImage: #imageLiteral(resourceName: "profile_unselected"),selectedImage: #imageLiteral(resourceName: "profile_selected"),rootViewController: ProfileVC(collectionViewLayout: profileLayout))

        viewControllers = [feed,search,imagesSelector,notification,profile]
        tabBar.tintColor = .black
    }
    
    func templateNavigationController(unselectedImage: UIImage, selectedImage: UIImage,rootViewController:UIViewController) -> UINavigationController{
        
        let nav = UINavigationController(rootViewController: rootViewController)
        
        nav.tabBarItem.image = unselectedImage
        nav.tabBarItem.selectedImage = selectedImage
        nav.navigationBar.tintColor = .black
        //nav.navigationBar.backgroundColor = .blue
        return nav
    }
}


