//
//  RegistrationVC.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/8/22.
//

import UIKit

class RegistrationVC: UIViewController {

    private var viewModel = RegistrationVewModel()
    private var profileImage: UIImage?
    
    //MARK: -  Properties
    
    private let plushPhotoButton:UIButton = {
        let bt = UIButton(type: .system)
        bt.setImage(UIImage(named: "plus_photo"), for: .normal)
        bt.tintColor = .white
        bt.addTarget(self, action:#selector(handleProfilePhotoSelect), for: .touchUpInside)
        return bt
    }()
    
    private let emailTextField:CustomTextField = {
        let tf = CustomTextField(placeholder: "Email")
        tf.keyboardType = .emailAddress
        
        return tf
        
    }()
    
    private let passwordTextField:CustomTextField = {
        let tf = CustomTextField(placeholder: "Password")
        tf.isSecureTextEntry = true
        return tf
        
    }()
    
    private let fullNameTExtField:CustomTextField = CustomTextField(placeholder: "Fullname")
    
    private let usernameTextField:CustomTextField = CustomTextField(placeholder: "Username")
    
    
    private let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: UIControl.State.normal)
        button.setTitleColor(UIColor(white: 1, alpha:0.68), for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor =  #colorLiteral(red: 0.8446559906, green: 0.5145556927, blue: 1, alpha: 1).withAlphaComponent(0.5)
        button.setHeight(50)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
        
    }()
    
    private let allreadyHaveAccountbutton: UIButton = {
      
        let button = UIButton(type: .system)
        button.attributedTitle(firstPart: "Already have account yet? ", secondPart: "Sign in. ")
        button.addTarget(self, action: #selector(handleShowLogin), for: .touchUpInside)
        return button
    }()
    
    //MARK: -  Lifecyccle
    override func viewDidLoad() {
        super.viewDidLoad()
        cofigureUI()
        configureNotificationObservers()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func handleShowLogin(){
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: -  Helpers
    func cofigureUI(){
        configureGradientLayer()
        
        view.addSubview(plushPhotoButton)
        plushPhotoButton.centerX(inView: view)
        plushPhotoButton.setDimensions(height: 100, width: 100)
        plushPhotoButton.anchor(top:view.safeAreaLayoutGuide.topAnchor,paddingTop: 32)
        
        let stack = UIStackView(arrangedSubviews: [emailTextField,passwordTextField,fullNameTExtField,usernameTextField,signUpButton])
        stack.axis = .vertical
        stack.spacing = 15
        
        view.addSubview(stack)
        stack.anchor(top:plushPhotoButton.bottomAnchor,left:view.leftAnchor,right: view.rightAnchor,paddingTop: 32,paddingLeft: 32, paddingRight: 32)
        
        view.addSubview(allreadyHaveAccountbutton)
        allreadyHaveAccountbutton.centerX(inView: view)
        allreadyHaveAccountbutton.anchor(bottom:view.safeAreaLayoutGuide.bottomAnchor)

        
    }
    
    func configureNotificationObservers(){
        
        emailTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        fullNameTExtField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        usernameTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        
    }
    
    @objc func textDidChange(sender:UITextField){
        if sender == emailTextField{
            viewModel.email = sender.text
        }else if sender == passwordTextField{
            viewModel.password = sender.text
        } else if sender == fullNameTExtField{
            viewModel.fullName = sender.text
        }else{
            viewModel.username = sender.text
        }
        
        updateForm()
        
    }
    
    @objc func handleProfilePhotoSelect(){
        print("DEBUG: show photo library")
        let picker  = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker,animated: true, completion: nil)
    }
    
    @objc func handleSignUp(){
        
        guard let email = emailTextField.text?.lowercased() else { return }
        guard let password = passwordTextField.text else { return }
        guard let fullname = fullNameTExtField.text else { return }
        guard let username = usernameTextField.text?.lowercased() else { return }
        guard let profileImage = self.profileImage else {return}
        
        let credentials = AuthCredentials(email: email, password: password, fullname: fullname, username: username, profileImage: profileImage)
    
        AuthService.registerUser(withCredentials: credentials){ error in
            
            if let error = error{
                print("DEBUG : \(error.localizedDescription)")
            }
            
            self.dismiss(animated: true,completion: nil)
        
        }
    }



}

extension RegistrationVC:FormViewModel{
    func updateForm() {
        signUpButton.backgroundColor = viewModel.backgroundColor
        signUpButton.setTitleColor(viewModel.buttonTitleColor, for: .normal)
        signUpButton.isEnabled = viewModel.formIsValid
    }
    
    
}

//MARK: -  ImagePicker
extension RegistrationVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[.editedImage] as? UIImage else { return }
        
        profileImage = selectedImage
        plushPhotoButton.layer.cornerRadius = plushPhotoButton.frame.width / 2
        plushPhotoButton.layer.masksToBounds = true
        plushPhotoButton.layer.borderColor = UIColor.white.cgColor
        plushPhotoButton.layer.borderWidth = 2
        plushPhotoButton.setImage(selectedImage.withRenderingMode(.alwaysOriginal), for: .normal)
        
        self.dismiss(animated: true,completion: nil)
        
    }
}
