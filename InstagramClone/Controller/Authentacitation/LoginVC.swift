//
//  LoginVC.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/8/22.
//

import UIKit

class LoginVC: UIViewController {
    
    private var viewModel = LoginViewModel()

    private let iconImage: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "Instagram_logo_white"))
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    private let emailTextField:CustomTextField = {
        let tf = CustomTextField(placeholder: "email")
        tf.keyboardType = .emailAddress
        tf.autocapitalizationType = .none
        return tf
        
    }()
    
    private let passwordTextField:CustomTextField = {
        let tf = CustomTextField(placeholder: "password")
       
        tf.isSecureTextEntry = true
        return tf
        
    }()
    
    private let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Log in", for: UIControl.State.normal)
        button.setTitleColor(UIColor(white: 1, alpha: 0.68), for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.8446559906, green: 0.5145556927, blue: 1, alpha: 1).withAlphaComponent(0.5)
        button.layer.cornerRadius = 5
        button.setHeight(50)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(UIColor(white: 1, alpha: 0.68), for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(handleSignInButton), for: .touchUpInside)
        return button
        
    }()
    
    private let dontHaveAccountbutton: UIButton = {
      
        let button = UIButton(type: .system)
        button.attributedTitle(firstPart: "Dont have account yet? ", secondPart: "Sign up. ")
        button.addTarget(self, action: #selector(handleShowSignUp), for: .touchUpInside)
        return button
    }()
    
    private let forgotPasswordButton: UIButton = {
    
        let button = UIButton(type: .system)
        button.attributedTitle(firstPart: "Forgot your password", secondPart: "Get help signing in. ")
                    
       
        return button
    }()

                
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        ConfigureUI()
        configureNotificationObservers()
        // Do any additional setup after loading the view.
    }
    

    @objc func handleShowSignUp(){
        let vc = RegistrationVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func handleSignInButton(){
        guard let email = emailTextField.text?.lowercased() else { return }
        guard let password = passwordTextField.text else { return }
        AuthService.logUser(withEmail: email, password: password) { result, error in
            if let error = error{
                print("DEBUG : log user in \(error.localizedDescription)")
            }else{
                self.dismiss(animated: true,completion: nil)
            }
            
            
        }
    }
    func ConfigureUI(){
        view.backgroundColor = .white
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isHidden = true
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.systemPurple.cgColor,UIColor.systemBlue.cgColor]
        gradient.locations = [0,1]
        view.layer.addSublayer(gradient)
        gradient.frame = view.frame
        
        view.addSubview(iconImage)
        iconImage.centerX(inView: view)
        iconImage.setDimensions(height: 80, width: 120)
        iconImage.anchor(top:view.safeAreaLayoutGuide.topAnchor,paddingTop: 32)
        
        let stack = UIStackView(arrangedSubviews: [emailTextField,passwordTextField,loginButton,forgotPasswordButton])
        stack.axis = .vertical
        stack.spacing = 20
        
        view.addSubview(stack)
        stack.anchor(top:iconImage.bottomAnchor,left: view.leftAnchor,right: view.rightAnchor,paddingLeft: 32,paddingRight: 32)
        
        
        view.addSubview(dontHaveAccountbutton)
        dontHaveAccountbutton.centerX(inView: view)
        dontHaveAccountbutton.anchor(bottom:view.safeAreaLayoutGuide.bottomAnchor)
        
    }
    
    func configureNotificationObservers(){
        
        emailTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        
    }
    
    @objc func textDidChange(sender:UITextField){
        if sender == emailTextField{
            viewModel.email = sender.text
        }else{
            viewModel.password = sender.text
        }
        
        updateForm()
        
    }

   

}

extension LoginVC:FormViewModel{
    func updateForm() {
        loginButton.backgroundColor = viewModel.backgroundColor
        loginButton.setTitleColor(viewModel.buttonTitleColor, for: .normal)
        loginButton.isEnabled = viewModel.formIsValid
    }
    
    
}
