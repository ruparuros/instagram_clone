//
//  FeedVc.swift
//  InstagramClone
//
//  Created by Uros Rupar on 4/6/22.
//

import UIKit
import Firebase

class FeedVc: UIViewController {
    
    @IBOutlet weak var feedCollection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.navigationController?.navigationBar.backgroundColor  = .red
        configureUI()
       // feedCollection.register(FeedCell.self, forCellWithReuseIdentifier: "FeedCell")
    }
    
    
    func configureUI(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target:self , action: #selector(handleLogOut))
    }
    
    @objc func handleLogOut(){
        do{
            try Auth.auth().signOut()
            let controller = LoginVC()
            let nav = UINavigationController(rootViewController: controller)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        }catch{
            print("greska")
        }
        
        

    }
    
}

    


extension FeedVc:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCell", for: indexPath) as! FeedCell
       // cell.profileName.text = "asas"
        return cell
    }
    
    
}

extension FeedVc:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: view.frame.height*0.7)
    }
}

